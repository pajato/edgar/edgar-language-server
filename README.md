# edgar-language-server

A test language server for the Edgar project. This extensible Language Server Protocol implementation is highly configurable via command line options to generate any and all conditions a LSP client might encounter.